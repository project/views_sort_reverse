<?php

/**
 * @file
 * Contains views hooks.
 */

declare(strict_types=1);

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function views_sort_reverse_views_data(): array {
  $data['views']['reverse'] = [
    'title' => new TranslatableMarkup('Reverse'),
    'help' => new TranslatableMarkup('Reverses the row order for pages.'),
    'sort' => [
      'id' => 'reverse',
    ],
  ];
  return $data;
}
