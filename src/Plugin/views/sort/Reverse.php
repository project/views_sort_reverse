<?php

declare(strict_types=1);

namespace Drupal\views_sort_reverse\Plugin\views\sort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Attribute\ViewsSort;
use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Views reverse sort handler.
 *
 * @ingroup views_sort_handlers
 */
#[ViewsSort('reverse')]
final class Reverse extends SortPluginBase {

  /**
   * {@inheritdoc}
   */
  public function canExpose(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $form['order']['#access'] = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function postExecute(&$values): void {
    $values = array_reverse($values);
  }

}
