# Views sort reverse

This module provides a sorting plugin for views that reverses page results.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Configuration

1. After installation, go to the settings of the desired view.
2. Start adding a filter.
3. Find and select "Reverse" from the list of options.
4. Submit the form.
